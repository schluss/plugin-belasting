const json = require('./template.json');
let attributeList = json.attributes;

/**
 * Reading XML File Content Processor.
 * 
 */
const XmlProcessor = {
    readFile: (fileLocation) => {
        return new Promise((resolve, reject) => {
            window.resolveLocalFileSystemURL(fileLocation, resolve, reject);
        }).then((fileEntry) => {
            return getContentFromFile(fileEntry);
        }).then((data) => {
            return getTemplateData(data);
        });
    }
};

/**
 * Read XML Content.
 * @param {string} fileEntry 
 * @returns {string} xmlContent
 */
function getContentFromFile(fileEntry) {
    return new Promise((resolve, reject) => {
        fileEntry.file((file) => {
            let reader = new FileReader();

            reader.onloadend = () => {
                console.log("reader.result  : " + reader.result);
                resolve(reader.result )
            };

            reader.onerror = () => {
                console.log("reader.error  : " + reader.error);

                reject(reader.error )
            };

            reader.readAsText(file);
        }, reject);
    });
}

/**
 * Process Content with Template
 * @param {String} result 
 */
function getTemplateData(result) {
    return new Promise((resolve, reject) => {
        let parser = new DOMParser();
        let xmlDoc = parser.parseFromString(result, "text/xml");
        let response = [];
        let returnList = {};

        for (i=0; i<attributeList.length; i++) {
            if (attributeList[i].visible == "true") {
                let attrId = attributeList[i].attibute_name;
                let x = xmlDoc.getElementsByTagName(attrId);
                let nodeValues = [];
                
                if(x.length>1){
                    for (j=0; j<x.length; j++){
                        let y = x[j].childNodes[0].nodeValue;
                        nodeValues.push(y);
                    }
                    response.push({"name":attributeList[i].attibute_name,"value": nodeValues});
                }else{
                    response.push({"name":attributeList[i].attibute_name,"value": x[0].childNodes[0].nodeValue});
                }
            }
        }
        returnList["payload"] = response;
        console.log("XML Response : " + JSON.stringify(returnList));

        resolve(JSON.stringify(returnList));
    });
}

module.exports = XmlProcessor;